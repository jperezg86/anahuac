
// var $ = jQuery.noConflict();

jQuery.fn.extend({
    animateCss: function (animationName) {
        var animationEnd = 'webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend';
        this.addClass('animated ' + animationName).one(animationEnd, function() {
            jQuery(this).removeClass('animated ' + animationName);
			jQuery(this).trigger("animationEnd");
        });
    }
});


var pub = jQuery('.to_fix');
var pub2 = jQuery('.to_fix_2');
var share = jQuery('.share');
var tope = 0;
var tope2 = 0;
var topeShare = 0;
var start = 0;
var start2 = 0;
var start3 = 0;




jQuery(document).ready(function() {
	jQuery('body').append('<div class="overlay"/>');
	var funResize = function(){
		var wdW = jQuery(window).width();
		var wdH = jQuery(window).height();
		if(wdW <= 736 && wdW > wdH){//moblandscape
			jQuery('body').removeAttr('class');
			jQuery('body').addClass('mobile-land');
				jQuery('.suscribe').insertBefore('nav > ul');
		}
		else if(wdW <= 736 && wdW < wdH){//mobportrait
			jQuery('body').removeAttr('class');
			jQuery('body').addClass('mobile-port');
				jQuery('.suscribe').insertBefore('nav > ul');
		}
		else if(wdW >= 736 && wdW < wdH){//tabletport
			jQuery('body').removeAttr('class');
			jQuery('body').addClass('tablet-port');
				jQuery('.suscribe').insertBefore('header form');
		}
		else if(wdW >= 736 && wdW > wdH && wdW < 1100){//tabletland
			jQuery('body').removeAttr('class');
			jQuery('body').addClass('tablet-land');
				jQuery('.suscribe').insertBefore('header form');
		}
		else{
			jQuery('body').removeAttr('class');
			jQuery('body').addClass('normalize');
				jQuery('.suscribe').insertBefore('header form');
		}
	};
	
	funResize();
	var headerFix = function(){
			var topH = jQuery('main').offset().top;
			var startH = topH;
			jQuery(window).on('scroll.header',function(){
				var scrolL = jQuery(window).scrollTop();
				if(scrolL >= startH){
					jQuery('main').css('padding-top',startH);
					jQuery('header').addClass('fixed animated fadeInDown nopub');
					
					if( jQuery('body').hasClass('mobile-port') ||  jQuery('body').hasClass('mobile-lands')){
					}
					else{
						jQuery('nav').addClass('fixed');
					}
				}
				else{
					if( jQuery('html').hasClass('mobile') ){
						jQuery('main').css('padding-top','inherit');
						jQuery('header').removeClass('fixed fadeInDown nopub');
						jQuery('nav').removeClass('fixed');
						jQuery('#progressbar').hide();
					}
					else{					
						jQuery('main').css('padding-top','inherit').removeClass('nopub');
						jQuery('header').removeClass('fixed animated fadeInDown slideToNav slideCloseNav nopub');
						jQuery('main,footer').removeClass('slideToNav');//.addClass('slideCloseNav');
						jQuery('nav').removeClass('fixed open');
						jQuery('.open_nav').removeClass('active');	
					}
				}
			});
	};
	
	if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) === true) {
		jQuery('html').addClass('mobile');
		
		/*jQuery('.menu-item-has-children > a').each(function(index,element){
			jQuery(this).click(function(e){
				if( jQuery(this).hasClass('active') ){
					window.location = this.href;
				}
				else{
					e.preventDefault();
					jQuery(this).siblings('ul').slideDown();
					jQuery(this).addClass('active');
				}
			})
		});*/
		
		var myInterval = setInterval(function() {
			if( jQuery('header .publicidad.movil iframe').length ){
				clearInterval(myInterval);
				jQuery('header .publicidad.movil').css('height', jQuery('header .publicidad.movil iframe').height() );		
			}
			else{
				//console.log('nohay');
			}
		}, 500);
		jQuery(window).on('touchmove', function() {
				jQuery('header').addClass('nopub');
		});
	}
	else{
		headerFix();
		var menuCopy = jQuery('nav .inner > ul').clone();
		console.log(menuCopy);
		menuCopy.appendTo('header menu');
		
		//pubfix
		if( jQuery('.note').length ){
			jQuery(window).on('scroll.pub',function(){	
				var scrollTop = jQuery(window).scrollTop();

				if(scrollTop >= start && scrollTop < tope ){
					pub.addClass('fixed');
					pub.removeClass('absolute');
					pub.css('margin-right',0);				
				}
				else if(scrollTop >= tope){
					pub.removeClass('fixed');
					pub.addClass('absolute');
					pub.removeAttr('style');
				}
				else{
					pub.removeClass('fixed');
					pub.removeAttr('style');
					pub.removeClass('absolute');
				}
			});
			
			/*jQuery(window).on('scroll.pub2',function(){	
				var scrollTop = jQuery(window).scrollTop();
				
				if(scrollTop >= start2 && scrollTop < tope2 ){
					pub2.addClass('fixed');
					pub2.removeClass('absolute');
					pub2.css('margin-right',0);				
				}
				else if(scrollTop >= tope2){
					pub2.removeClass('fixed');
					pub2.addClass('absolute');
					pub2.removeAttr('style');
				}
				else{
					pub2.removeClass('fixed');
					pub2.removeAttr('style');
					pub2.removeClass('absolute');
				}
			});*/
			
			jQuery(window).on('scroll.share',function(){	
				var scrollTop = jQuery(window).scrollTop();
				
				if(scrollTop >= start2 && scrollTop < topeShare){
					share.addClass('fixed');
					share.removeClass('absolute');
					share.css('margin-right',0);
				}
				else if(scrollTop >= topeShare){
					share.removeClass('fixed');
					share.addClass('absolute');
					share.removeAttr('style');
				}
				else{
					share.removeClass('fixed');
					share.removeAttr('style');
					share.removeClass('absolute');
				}
			});
			
				function onElementHeightChange(elm, callback){
					var lastHeight = elm.clientHeight, newHeight;
					(function run(){
						newHeight = elm.clientHeight;
						if( lastHeight !== newHeight ){
							callback();
							lastHeight = newHeight;
						}
				
						if( elm.onElementHeightChangeTimer ){
							clearTimeout(elm.onElementHeightChangeTimer);
						}
						elm.onElementHeightChangeTimer = setTimeout(run, 200 );
					})();
				}
				onElementHeightChange(document.body, function(){
					
					console.log('ef');
					
					jQuery('.to_fix').removeClass('fixed');
					jQuery('.share').removeClass('fixed');
					
					if( jQuery('.share').length ){
						share = jQuery('.share');
						topeShare = jQuery('.col_aside').offset().top + jQuery('.col_aside').height() - 240;
						start2 = jQuery('.share').offset().top - 95;
					}
					if( jQuery('.to_fix').length ){
						
						pub = jQuery('.to_fix');
						start = jQuery('.to_fix').offset().top - 95;
						tope = jQuery('aside').height() + jQuery('aside').offset().top - pub.height() - 90;
					}
					
					
				
				});
			
				jQuery(window).trigger('onElementHeightChange',true);
			
			
		}//note length
	}//eventos desktop
	
	//resizeEvt
	var resizeTimer;
	jQuery(window).on('resize', function() {
	  clearTimeout(resizeTimer);
	  resizeTimer = setTimeout(function() {	
			// Run code here, resizing has "stopped"
			funResize();
			jQuery('header').removeClass('fixed animated slideInDown');
			jQuery('main').removeAttr('style');
	  }, 250);
	});
	
	
	jQuery('.open_search,.search .close').click(function(e){
		e.preventDefault();
		if( jQuery('.open_search').hasClass('active') ){
			jQuery('.open_search').removeClass('active');
			jQuery('.search').animateCss('fadeOut');
			setTimeout(function(){
				jQuery('.search').hide().removeClass('animated fadeOut');
			},800);
			jQuery('.search input:hidden').trigger('blur');
		}
		else{
			jQuery('.open_search').addClass('active');
			jQuery('.search').show().animateCss('fadeIn');
			jQuery('.search input:visible').focus();
		}
	});
	
	jQuery('.overlay').click(function(e){
		e.preventDefault();
		
	});
	
	
	
	jQuery('.open_nav').click(function(e){
		e.preventDefault();
		if( jQuery(this).hasClass('active') ){
			jQuery('main,header,footer').removeClass('slideToNav').addClass('slideCloseNav animated');
			setTimeout(function(){
				jQuery('main,header,footer').removeClass('slideCloseNav animated');
			},1000);
			jQuery('nav').removeClass('open');
			jQuery(this).removeClass('active');
		}
		else{
			jQuery('main,header,footer').addClass('slideToNav animated').removeClass('slideCloseNav');
			jQuery('nav').addClass('open');
			jQuery(this).addClass('active');
		}	
		jQuery('header').removeClass('slideInDown');
	});
	
	
	jQuery('.overlay.menu_nav').click(function(e){
		e.preventDefault();
		jQuery('html').removeClass('menu_open');
		jQuery(this).removeClass('opened');
		jQuery('.open_nav i').addClass('fa-bars');
		jQuery('.open_nav i').removeClass('fa-times');
	});
	
	
	// var carouselHome = jQuery('.carousel_home');
	// carouselHome.addClass('owl-carousel');
	// carouselHome.owlCarousel({
	// 	margin: 20,
	// 	loop: true,
	// 	responsive:{
	// 		0: {
	// 			items: 1,
	// 			margin:10
	// 		},
	// 		450:{
	// 			items: 3
	// 		}
	// 	},
	// 	center:false,
	// 	//autoplay:true,
	// 	nav:true,
	// 	autoHeight:false,
	// });
	
	
	
	
	// var carouselEvents = jQuery('.carousel_events');
	// carouselEvents.addClass('owl-carousel');
	// carouselEvents.owlCarousel({
	// 	margin: 20,
	// 	loop: false,
	// 	responsive:{
	// 		0: {
	// 			items: 1,
	// 			margin:10
	// 		},
	// 		450:{
	// 			items: 3
	// 		}
	// 	},
	// 	center:false,
	// 	nav:true,
	// 	autoHeight:false,
	// });
	
	
	
	if( jQuery('.gallery').length ){
		var gallery = jQuery('.gallery');
		gallery.owlCarousel({
			margin: 0,
			loop: false,
			responsive:{
				0: {
					items: 1,
				},
				450:{
					items: 1
				}
			},
			responsiveBaseElement:".mainImg",
			center:false,
			autoplay:false,
			nav:true,
			autoHeight:true,
			singleItem:true
		});
		jQuery('.gallery img').each(function() {
			var imgSlide = jQuery(this).attr('src');
			jQuery(this).parent().css({
				'background-image': 'url("'+imgSlide+'")',
			});
		});
		jQuery('.gallery article').each(function(index) {
			var ind = index+1;
			var total = jQuery('.gallery img').length;
			jQuery(this).append('<span>'+ind+' / '+total+'</span>');
		});
		
		jQuery('.expand_gallery').click(function(e){
			e.preventDefault();
			jQuery(this).children('i').toggleClass('fa-expand fa-compress');
			jQuery('.mainImg').toggleClass('full');
			gallery.trigger('destroy.owl.carousel');
			gallery.html(gallery.find('.owl-stage-outer').html()).removeClass('owl-loaded');
			gallery.owlCarousel({
				margin: 0,
				loop: true,
				items:1,
				center:true,
				autoplay:false,
				nav:true,
			});
		});
	}
	
	
	
	jQuery('.tabs_go').each(function(){
		var tabs = jQuery(this).children('.content_choose').children('section');
		jQuery(this).children('.choose').children('a').click(function(e){
			jQuery(this).siblings('a').removeClass('active');
			e.preventDefault();
			jQuery(this).addClass('active');
			tabs.removeClass('active');
			tabs.filter(this.hash).addClass('active');		
		}).filter(':first').click();
	});
	
	
	
	jQuery('.collapsable h3').each(function() {
		jQuery(this).click(function(){
			if( jQuery(this).hasClass('active') ){					
				jQuery(this).next('div').slideUp();
				jQuery(this).removeClass('active');
				jQuery(this).children('i').removeClass('fa-angle-up');	
			}
			else{
				jQuery('.collapsable h3').removeClass('active');
				jQuery('.collapsable h3 i').removeClass('fa-angle-up');
				jQuery('.collaps').slideUp();
				jQuery(this).next('div').slideDown();
				jQuery(this).addClass('active');
				jQuery(this).children('i').addClass('fa-angle-up');
			}
		});
	});

	
	//lightboxes
	jQuery('.light_newsletter').click(function(e){
		e.preventDefault();
		jQuery('.lightbox.newsletter').lightbox_me({
			centered: true,
			onLoad: function(){
				if( jQuery('.overlay:has(.opened)') ){
					jQuery('.overlay').trigger('click');
				}
			},
        });
	});
	
	 
	jQuery('.top_section_img img').each(function() {
		var imgSlide = jQuery(this).attr('src');
		jQuery(this).parent().css({
			'background-image': 'url("'+imgSlide+'")',
		});
	});

	jQuery('.scrollTo').click(function(e){
		e.preventDefault();
		var goto = jQuery(this).attr('href');
		
		jQuery('html, body').animate({
			scrollTop: ( jQuery(goto).offset().top - 300 ),
		}, 1000);
	});

	jQuery('#edit-field-campus-del-evento-target-id').on('change',function(){
		jQuery('#edit-submit-agenda').click();
	});

});

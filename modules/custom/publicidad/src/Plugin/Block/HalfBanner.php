<?php 
namespace Drupal\publicidad\Plugin\Block;
use Drupal\Core\Block\BlockBase;

/**
* Provides a user details block.
*
* @Block(
* id = "half_banner",
* admin_label = @Translation("Un Half Banner")
* )
*/
class HalfBanner extends BlockBase {
	/**
	* {@inheritdoc}
	*/
	public function build() {
		return array(
			"#theme" => "half_banner",
			'#module_path' => base_path().drupal_get_path("module", 'publicidad')
		);
	}
}
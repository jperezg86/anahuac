<?php 
namespace Drupal\publicidad\Plugin\Block;
use Drupal\Core\Block\BlockBase;

/**
* Provides a user details block.
*
* @Block(
* id = "leader_banner",
* admin_label = @Translation("Un box Banner")
* )
*/
class LeaderBanner extends BlockBase {
	/**
	* {@inheritdoc}
	*/
	public function build() {
		return array(
			"#theme" => "leader_banner",
			'#module_path' => base_path().drupal_get_path("module", 'publicidad')
		);
	}
}
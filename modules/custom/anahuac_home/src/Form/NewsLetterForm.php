<?php 
namespace Drupal\anahuac_home\Form;


use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;

use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

/**
* Implementa un form para suscribirse al Newslettter
*/
class NewsLetterForm extends FormBase{

	/**
	* {@inheritdoc}
	**/

	public function getFormId(){
		return 'newsletter_form';
	}

	/**
   	* {@inheritdoc}
   	*/
	public function buildForm(array $form, FormStateInterface $form_state) {

		$form['nombre'] = array(
			'#type' => 'textfield',
			'#title' => $this->t('Nombre'),
			'#default_value' => "JOSE MANUEL",
			'#placeholder' => $this->t('Nombre'),
			'#title_display' => 'invisible',
			'#theme'=> 'name_field'
		);

		$form['email'] = array(
			'#type' => 'email',
			'#title' => $this->t('Correo electrónico'),
			'#default_value' => "jperezg86@gmail.com",
			'#placeholder' => $this->t('Ingresa tu correo electrónico'),
			'#title_display' => 'invisible',
			'#theme' => 'email_field'
		);

		$form['actions'] = array(
  			'#type' => 'actions',
		);

		
		$form['actions']['submit'] = array(
			'#value' => t('Enviar'),
  			'#type' => 'submit',
			'#ajax' => array('callback' => '::sendSubscription')
		);

		$form['#theme'] = 'newsletter_form';

		return $form;
  	}

  	/**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

  public function sendSubscription(array &$form, FormStateInterface $form_state){
  	 $responseText = "JUANITO JOHNS";
	 if(empty($form_state->getValue('nombre'))){
			$responseText = $this->t('El campo Nombre es obligatorio');
	}else if(!filter_var($form_state->getValue('email'), FILTER_VALIDATE_EMAIL)){
		$responseText = $this->t('Debe introducir un email con formato válido');
	}else{
		$result = $this->sendEmail($form_state->getValue('nombre'),$form_state->getValue('email'));
		if ($result === true) {
			$responseText = '¡Gracias por suscribirte!';
 		}else {
		   $responseText = 'Ocurrió un error con el envío de correo, por favor intente mas tarde';
		}
	}
	$ajax_response = new AjaxResponse();
	$ajax_response->addCommand(new HtmlCommand('#error-messages', $responseText));
	return $ajax_response;
  }


  public function sendEmail($nombre,$email){
	  	$mail = new PHPMailer(true);                              // Passing `true` enables exceptions
		try {
		    //Server settings
		    // $mail->SMTPDebug = 2;                                 // Enable verbose debug output
		    $mail->isSMTP();    

		    $mail->Host = 'mail.tecprosolutions.com.mx';  // Specify main and backup SMTP servers
		    $mail->SMTPAuth = true;                               // Enable SMTP authentication
		    $mail->Username = 'jperez@tecprosolutions.com.mx';                 // SMTP username
		    $mail->Password = 'dw2O~xgGB+DJ';                           // SMTP password
		    // $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
		    $mail->Port = 26;                                    // TCP port to connect to

		    //Recipients
		    $mail->setFrom('jperez@tecprosolutions.com.mx', 'Test Mailer');
		    // $mail->addAddress('jperezg86@gmail.com', 'Jose Manuel');     // Add a recipient
		    $mail->addAddress('eduardo.scheffler6@gmail.com', 'Eduardo');     // Add a recipient
		    $mail->AddCC('jperezg86@gmail.com');
		    // $mail->addAddress('ellen@example.com');               // Name is optional
		    $mail->addReplyTo('contacto@tecprosolutions.com.mx', 'Information');

		    //Content
		    $mail->isHTML(true);                                  // Set email format to HTML
		    $mail->Subject = '[GENERACION_ANAHUAC][SUSCRIPCION]';
		    $mail->Body    = 'Por favor registre este usuario al newsletter de Generación Anáhuac: <br/><b>Nombre:</b>'.$nombre.'<br/><b>email:</b>'.$email;
		    $mail->AltBody = 'Por favor registre este usuario al newsletter de Generación Anáhuac: NOMBRE: '.$nombre.' EMAIL: '. $email;
		    $mail->send();
		} catch (Exception $e) {
		    return false;
		}

		return true;
	}


}
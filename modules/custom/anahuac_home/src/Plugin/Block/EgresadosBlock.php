<?php 
namespace Drupal\anahuac_home\Plugin\Block;
use Drupal\Core\Block\BlockBase;

/**
* Provides a user details block.
*
* @Block(
* id = "home_egresados_block",
* admin_label = @Translation("Bloque de Egresados en el Home")
* )
*/
class EgresadosBlock extends BlockBase {
	/**
	* {@inheritdoc}
	*/
	public function build() {
		return array(
			"#theme" => "home_egresados",
			"#egresados" => $this->getEgresados()
			// '#eventos' => $this->getEventos()
		);
	}


	private function getEgresados(){
		$nodes = array();
		$queryDestacado = \Drupal::entityQuery('node')
				->condition('status',1)
				->condition('type','egresados')
				->condition('promote',1)
				->range(0,1)
				->sort('changed','DESC');
		$destacados = $queryDestacado->execute();
		$destacadosNodes = node_load_multiple($destacados);
		$nodeDestacado = array_shift($destacadosNodes);
		array_push($nodes,$nodeDestacado);

		// $query = db_select('field_data_field_category', 'c')
		// ->fields('c', array('entity_id'))
		// -> join('node', 'n', 'n.nid = entity_id')
		// -> condition('n.status', 1,'>')
		// -> condition('c.field_category_tid', $node->field_category['und'][0]['tid']);
	

		$query = db_select('node','n');
		$query->fields('n',['nid']);
		$query->innerJoin('node_field_data','nfd', 'n.nid = nfd.nid');
		$query->condition('n.type','egresados','LIKE')
				->condition('n.nid',array($nodeDestacado->id()),'NOT IN')
				->condition('nfd.status', '1',"=")
				// ->orderRandom()
				->range(0,16);
		$query->orderBy('n.nid')->orderRandom();
		$nids=$query->execute()->fetchCol(0);
		$nodesRnd = node_load_multiple($nids);

		foreach ($nodesRnd as $node) {
			array_push($nodes,$node);
		}
		return $nodes;
	}
}
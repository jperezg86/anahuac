<?php 
namespace Drupal\anahuac_home\Plugin\Block;
use Drupal\Core\Block\BlockBase;

/**
* Provides a user details block.
*
* @Block(
* id = "home_latest_notes_block",
* admin_label = @Translation("Bloque de lo último")
* )
*/
class LatestNotesBlock extends BlockBase {
	/**
	* {@inheritdoc}
	*/
	public function build() {

	/**
	*Carga bloque de boxbanner
	**/
	$box = \Drupal::service('plugin.manager.block')->createInstance('box_banner', []);
	$boxBanner = $box->build();
		return array(
			"#theme" => "latest_notes",
			'#items_latest' => $this->getLatestNotes(),
			'#module_path' => drupal_get_path("module", 'anahuac_home'),
			'#box_banner' => $boxBanner
		);
	}


	private function getLatestNotes(){
		$notes = array();
		$query = \Drupal::entityQuery('node')
			->condition('status',1)
			->condition('type','article')
			->condition('promote',1)
			->range(5,3)
			->sort('changed','DESC');
		 $nids = $query->execute();
		 $nodes = entity_load_multiple('node',$nids);
		 return $nodes;
	}
}
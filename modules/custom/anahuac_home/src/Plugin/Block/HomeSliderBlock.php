<?php
namespace Drupal\anahuac_home\Plugin\Block;
use Drupal\Core\Block\BlockBase;

/**
* Provides a user details block.
*
* @Block(
* id = "home_slider_block",
* admin_label = @Translation("Home Slider Block!")
* )
*/
class HomeSliderBlock extends BlockBase {
	/**
	* {@inheritdoc}
	*/
	public function build() {
		return array(
			"#theme" => "home_slider",
			'#items_slider' => $this->getSliderNodes()
		);
	}

	private function getSliderNodes(){
		$notes = array();
		$query = \Drupal::entityQuery('node')
			->condition('status',1)
			->condition('type',array('article','egresados'),"IN")
			->condition('promote',1)
			->range(0,5)
			->sort('changed','DESC');
		 $nids = $query->execute();
		 $nodes = entity_load_multiple('node',$nids);
		 foreach ($nodes as $node) {
				array_push($notes,$node);
		 }

		 return $notes;
	}
}
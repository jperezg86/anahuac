<?php 
namespace Drupal\anahuac_home\Plugin\Block;
use Drupal\Core\Block\BlockBase;

/**
* Provides a user details block.
*
* @Block(
* id = "home_agenda_block",
* admin_label = @Translation("Bloque de Agenda Anahuac")
* )
*/
class AgendaBlock extends BlockBase {
	/**
	* {@inheritdoc}
	*/
	public function build() {
		return array(
			"#theme" => "home_agenda",
			'#eventos' => $this->getEventos()
		);
	}


	private function getEventos(){
		$query = db_select('node','n');
		$query->fields('n',['nid'])
				// ->condition('status',0,">")
				->condition('n.type','evento','LIKE')
				->orderRandom()
				->range(0,5);
		$nids=$query->execute()->fetchCol(0);
		$nodesRnd = node_load_multiple($nids);
		return $nodesRnd;


		// $notes = array();
		// $query = \Drupal::entityQuery('node')
		// 	->condition('status',1)
		// 	->condition('type','evento')
		// 	->range(0,5)
		// 	->sort('changed','DESC');
		//  $nids = $query->execute();
		//  $nodes = entity_load_multiple('node',$nids);
		//  foreach ($nodes as $node) {
		// 		array_push($notes,$node);
		//  }
		//  return $notes;
	}
}
<?php
namespace Drupal\anahuac_home\Plugin\Block;
use Drupal\Core\Block\BlockBase;

/**
* Provides a user details block.
*
* @Block(
* id = "home_videos_block",
* admin_label = @Translation("Home Videos Block!")
* )
*/
class VideosBlock extends BlockBase {
	/**
	* {@inheritdoc}
	*/
	public function build() {
		return array(
			"#theme" => "home_videos_block",
			'#videos' => $this->getVideosDestacados()
		);
	}

	private function getVideosDestacados(){
		$notes = array();
		$query = \Drupal::entityQuery('node')
			->condition('status',1)
			->condition('type','video')
			->condition('promote',1)
			->range(0,5)
			->sort('changed','DESC');
		 $nids = $query->execute();
		 $nodes = entity_load_multiple('node',$nids);
		 foreach ($nodes as $node) {
				array_push($notes,$node);
		 }
		 return $notes;
	}
}
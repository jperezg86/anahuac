<?php 
namespace Drupal\anahuac_home\Plugin\Block;
use Drupal\Core\Block\BlockBase;

/**
* Provides a user details block.
*
* @Block(
* id = "simple_carousel",
* admin_label = @Translation("Bloque de prueba de un owl carousel basico")
* )
*/
class SimpleCarousel extends BlockBase {
	/**
	* {@inheritdoc}
	*/
	public function build() {
		return array(
			"#theme" => "test-carousel"
		);
	}

}
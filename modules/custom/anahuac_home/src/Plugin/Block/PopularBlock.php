<?php 
namespace Drupal\anahuac_home\Plugin\Block;
use Drupal\Core\Block\BlockBase;

/**
* Provides a user details block.
*
* @Block(
* id = "home_popular_block",
* admin_label = @Translation("Bloque de Agenda Anahuac")
* )
*/
class PopularBlock extends BlockBase {
	/**
	* {@inheritdoc}
	*/
	public function build() {
		/**
		*Carga bloque de boxbanner
		**/
		$half = \Drupal::service('plugin.manager.block')->createInstance('half_banner', []);
		$half_banner = $half->build();

		return array(
			"#theme" => "home_popular_block",
			'#nodes' => $this->getPopularNodes(),
			'#half_banner' => $half_banner
		);
	}


	public function getPopularNodes(){
		$nodes = array();
		$query = db_select('node_counter','nc');
		$aliastmp = $query->innerJoin('node','n','nc.nid=n.nid');
		$query->fields('nc',['nid'])
				->condition('n.type','article','LIKE')
				->orderBy('nc.totalcount','DESC')
				->range(0,6);
		return entity_load_multiple('node',$query->execute()->fetchCol()); 
	}
}
jQuery(document).ready(function() {
	var carouselHome = jQuery('.carousel_home');
	carouselHome.addClass('owl-carousel');
		carouselHome.owlCarousel({
			margin: 20,
			loop: true,
			responsive:{
				0: {
					items: 1,
					margin:10
				},
				450:{
					items: 3
				}
			},
			center:false,
			//autoplay:true,
			nav:true,
			autoHeight:false,
			smartSpeed: 900, 
			autoplay:true,
			autoplayTimeout:6000,
			autoplayHoverPause:true
	});
});
jQuery(document).ready(function() {
	var carouselEvents = jQuery('.carousel_events');
	carouselEvents.addClass('owl-carousel');
	carouselEvents.owlCarousel({
		margin: 20,
		loop: false,
		responsive:{
			0: {
				items: 1,
				margin:10
			},
			450:{
				items: 3
			}
		},
		center:false,
		nav:true,
		autoHeight:false,
		smartSpeed: 900,
		autoplay:true,
		autoplayTimeout:6000,
		autoplayHoverPause:true
	});
});
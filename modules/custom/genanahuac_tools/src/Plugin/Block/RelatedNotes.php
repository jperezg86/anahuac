<?php 
namespace Drupal\genanahuac_tools\Plugin\Block;
use Drupal\Core\Block\BlockBase;

/**
* Provides a user details block.
*
* @Block(
* id = "related_notes",
* admin_label = @Translation("Bloque de notas relacionadas dentro de un bloque")
* )
*/
class RelatedNotes extends BlockBase {
	/**
	* {@inheritdoc}
	*/
	public function build() {
		return array(
			"#theme" => "related_notes",
			'#related_nodes' => $this->getRelatedNotes()
		);
	}


	public function getRelatedNotes(){
		$currentNode = $this->getConfiguration()['node'];
		$query = db_select('node','n');
		$query->fields('n',['nid'])
				->condition('n.type',$currentNode->getType())
				->condition('n.nid', $currentNode->id(),'<>')
				->orderRandom()
				->range(0,3);
		$nids=$query->execute()->fetchCol(0);
		$nodes = node_load_multiple($nids);
		 return $nodes;
	}
}
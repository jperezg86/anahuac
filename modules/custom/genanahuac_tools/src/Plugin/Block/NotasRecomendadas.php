<?php 
namespace Drupal\genanahuac_tools\Plugin\Block;
use Drupal\Core\Block\BlockBase;

/**
* Provides a user details block.
*
* @Block(
* id = "notas_recomendadas",
* admin_label = @Translation("Bloque de notas recomendadas")
* )
*/
class NotasRecomendadas extends BlockBase {
	/**
	* {@inheritdoc}
	*/
	public function build() {
		return array(
			"#theme" => "notas_recomendadas",
			'#recomendadas' => $this->getPopularNodes()
		);
	}


	public function getPopularNodes(){
		$currentNode = $this->getConfiguration()['node'];
		$nodes = array();
		$query = db_select('node_counter','nc');
		$aliastmp = $query->innerJoin('node','n','nc.nid=n.nid');
		$query->fields('nc',['nid'])
				->condition('n.type','article','LIKE')
				->condition('n.nid',$currentNode->id(),'<>')
				->orderBy('nc.totalcount','DESC')
				->range(0,3);
		return node_load_multiple($query->execute()->fetchCol()); 
	}
}